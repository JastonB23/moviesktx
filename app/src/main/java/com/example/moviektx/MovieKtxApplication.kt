package com.example.moviektx

import android.app.Application
import com.example.moviektx.ui.di.appModule
import com.example.moviektx.ui.di.topRatedModule
import org.koin.android.ext.android.startKoin

class MovieKtxApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin(this, listOf(appModule, topRatedModule))
    }
}