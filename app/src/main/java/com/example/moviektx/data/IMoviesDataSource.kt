package com.example.moviektx.data

interface IMoviesDataSource {
    suspend fun getTopRatedMovies() : MoviesResult
}