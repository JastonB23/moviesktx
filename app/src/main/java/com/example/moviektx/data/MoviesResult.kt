package com.example.moviektx.data

import com.example.model.local.MovieEntity

open class MoviesResult {
    object Error : MoviesResult()
    data class Success(val movies: List<MovieEntity>) : MoviesResult()
}