package com.example.moviektx.data

import com.example.model.local.MovieEntity

open class MoviesStatus {
    object Error : MoviesStatus()
    object Loading : MoviesStatus()
    data class Data(val movies: List<MovieEntity>) : MoviesStatus()
    data class SelectedMovie(val movie: MovieEntity) : MoviesStatus()
}