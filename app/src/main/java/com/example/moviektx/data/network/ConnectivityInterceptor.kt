package com.example.moviektx.data.network

import okhttp3.Interceptor

interface ConnectivityInterceptor : Interceptor
