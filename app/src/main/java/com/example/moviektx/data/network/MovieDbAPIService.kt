package com.example.moviektx.data.network

import com.example.moviektx.BuildConfig
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import kotlinx.coroutines.Deferred
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

const val BASE_URL: String = "https://api.themoviedb.org/3/"

interface MovieDbAPIService {

    companion object {
        // This is a static method for MovieDbAPIService
        operator fun invoke(
                connectivityInterceptor: ConnectivityInterceptor
        ): MovieDbAPIService {
            // Modifies URL before it's fired
            val requestInterceptor = Interceptor { chain ->
                val url = chain.request().url()
                        .newBuilder()
                        .addQueryParameter("api_key", BuildConfig.moviedata_apikey)
                        .build()

                val request = chain.request()
                        .newBuilder().url(url)
                        .build()

                return@Interceptor chain.proceed(request)
            }

            // Deal with client requests, adds the interceptor
            val okHttpClient = OkHttpClient.Builder()
                    .addInterceptor(requestInterceptor)
                    .addInterceptor(connectivityInterceptor)
                    .build()

            // May us DI to deal with this later on...
            return Retrofit.Builder()
                    .client(okHttpClient)
                    .baseUrl(BASE_URL)
                    .addCallAdapterFactory(CoroutineCallAdapterFactory())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
                    .create(MovieDbAPIService::class.java)
        }
    }

    @GET("movie/top_rated")
    fun getTopRatedMovies(): Deferred<Response<MovieEntityResponse>>
}