package com.example.moviektx.data.network

import com.example.model.local.MovieEntity
import com.google.gson.annotations.SerializedName

data class MovieEntityResponse(
    @SerializedName("page")
        val page: Int,
    @SerializedName("results")
        val results: List<MovieEntity>,
    @SerializedName("total_pages")
        val total_pages: Int,
    @SerializedName("total_results")
        val total_results: Int
) : Any()