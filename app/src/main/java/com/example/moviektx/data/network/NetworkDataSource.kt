package com.example.moviektx.data.network

import com.example.moviektx.data.MoviesResult
import com.example.moviektx.data.network.internal.NoConnectivityException

class NetworkDataSource(private val movieDbAPIService: MovieDbAPIService) {

    suspend fun getTopRatedMovies() : MoviesResult =
        try {
            val response = movieDbAPIService.getTopRatedMovies().await()
            MoviesResult.Success(response.body()?.results!!)
        } catch (e: NoConnectivityException) {
            MoviesResult.Error
        }
}