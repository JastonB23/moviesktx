package com.example.moviektx.data.network.internal

import java.io.IOException

class NoConnectivityException(message: String) : IOException(message)