package com.example.moviektx.data.repository

import com.example.moviektx.data.IMoviesDataSource
import com.example.moviektx.data.MoviesResult
import com.example.moviektx.data.network.NetworkDataSource
import org.threeten.bp.ZonedDateTime

class MoviesRepositoryImpl(private val networkDataSource: NetworkDataSource
) : BaseRepository(), IMoviesDataSource {

    override suspend fun getTopRatedMovies() : MoviesResult {
        return networkDataSource.getTopRatedMovies()
    }

    private fun isFetchDataRequired(previousTime: ZonedDateTime) : Boolean {
        val isThirtyMinAgo = ZonedDateTime.now().minusMinutes(30)
        return previousTime.isBefore(isThirtyMinAgo)
    }
}