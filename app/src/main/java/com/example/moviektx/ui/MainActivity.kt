package com.example.moviektx.ui

import android.os.Bundle
import android.util.Log
import android.view.View
import com.google.android.material.bottomnavigation.BottomNavigationView
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import com.example.common.base.SharedViewModel
import com.example.moviektx.R
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : AppCompatActivity() {

    private val sharedViewModel: SharedViewModel by viewModel()

    private lateinit var currentNavController: NavController
    private lateinit var navHostFragment: NavHostFragment

    private var navHostFragmentId: Int = R.id.nav_host_fragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        bottom_nav_bar.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
    }

    override fun onResume() {
        super.onResume()
        determineNavigationController(bottom_nav_bar.selectedItemId)
    }

    override fun onSupportNavigateUp(): Boolean {
        return if (getBackStackCount() > 0) {
            currentNavController.navigateUp()
        } else super.onSupportNavigateUp()
    }

    override fun onBackPressed() {
        if (getBackStackCount() > 0) {
            currentNavController.navigateUp()
        } else super.onBackPressed()
    }

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        determineNavigationController(item.itemId)
        true
    }

    private fun determineNavigationController(selectedTabId: Int) {
        var isSectionOneVisible = false
        var isSectionTwoVisible = false
        var isSectionThreeVisible = false

        when(selectedTabId) {
            R.id.home -> {
                Log.i("MainActivity", "Top Rated Tab")
                navHostFragmentId = R.id.nav_host_fragment
                isSectionOneVisible = true
            }
            R.id.favourites -> {
                Log.i("MainActivity", "Favourite Tab")
                navHostFragmentId = R.id.nav_host_fragment_two
                isSectionTwoVisible = true
            }
            R.id.settings -> {
                Log.i("MainActivity", "Settings Tab")
                navHostFragmentId = R.id.nav_host_fragment_three
                isSectionThreeVisible = true
            }
        }

        section_one_container.visibility = if (isSectionOneVisible) View.VISIBLE else View.GONE
        section_two_container.visibility = if (isSectionTwoVisible) View.VISIBLE else View.GONE
        section_three_container.visibility = if (isSectionThreeVisible) View.VISIBLE else View.GONE

        setNavigationHost(navHostFragmentId)
    }

    private fun setNavigationHost(navId: Int) {
        currentNavController = findNavController(navId)

        navHostFragment = supportFragmentManager.findFragmentById(navId) as NavHostFragment

        setPrimaryNavFragment(navHostFragment)

        //NavigationUI.setupActionBarWithNavController(this, currentNavController)
    }

    private fun setPrimaryNavFragment(navHostFrag: NavHostFragment) {
        supportFragmentManager.beginTransaction()
                .setPrimaryNavigationFragment(navHostFrag)
                .commit()
    }

    private fun getBackStackCount(): Int {
        return navHostFragment.childFragmentManager.backStackEntryCount
    }
}
