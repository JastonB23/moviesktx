package com.example.moviektx.ui.di

import com.example.common.base.SharedViewModel
import com.example.moviektx.data.network.ConnectivityInterceptor
import com.example.moviektx.data.network.ConnectivityInterceptorImpl
import com.example.moviektx.data.network.MovieDbAPIService
import com.example.moviektx.data.IMoviesDataSource
import com.example.moviektx.data.network.NetworkDataSource
import com.example.moviektx.data.repository.MoviesRepositoryImpl
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module

val appModule = module {
    single<ConnectivityInterceptor> { ConnectivityInterceptorImpl(androidContext()) }
    single { MovieDbAPIService(get()) }
    single { NetworkDataSource(get()) }
    single<IMoviesDataSource> { MoviesRepositoryImpl(get()) }
    viewModel { SharedViewModel() }
}