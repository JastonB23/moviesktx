package com.example.moviektx.ui.di

import com.example.moviektx.ui.movies.toprated.list.TopRatedMoviesAdapter
import com.example.moviektx.ui.movies.toprated.TopRatedMoviesViewModel
import com.example.moviektx.ui.movies.toprated.TopRatedViewModelFactory
import org.koin.androidx.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module

val topRatedModule = module {
    single { TopRatedViewModelFactory(get()) }
    viewModel { TopRatedMoviesViewModel(get()) }
    factory { TopRatedMoviesAdapter() }
}