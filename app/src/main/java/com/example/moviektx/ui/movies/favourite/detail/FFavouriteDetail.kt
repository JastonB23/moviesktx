package com.example.moviektx.ui.movies.favourite.detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders

import com.example.moviektx.R

class FFavouriteDetail : Fragment() {

    companion object {
        fun newInstance() = FFavouriteDetail()
    }

    private lateinit var viewModel: FavouriteDetailViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.favourite_detail_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(FavouriteDetailViewModel::class.java)
        // TODO: Use the ViewModel
    }

}
