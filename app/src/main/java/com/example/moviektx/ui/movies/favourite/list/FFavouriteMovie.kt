package com.example.moviektx.ui.movies.favourite.list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders

import com.example.moviektx.R

class FFavouriteMovie : Fragment() {

    companion object {
        fun newInstance() = FFavouriteMovie()
    }

    private lateinit var viewModel: FavouriteMovieViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.favourite_movie_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(FavouriteMovieViewModel::class.java)
        // TODO: Use the ViewModel
    }

}
