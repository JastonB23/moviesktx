package com.example.moviektx.ui.movies.toprated

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.example.moviektx.data.IMoviesDataSource
import com.example.moviektx.data.MoviesResult
import com.example.moviektx.data.MoviesStatus
import com.example.common.base.BaseViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class TopRatedMoviesViewModel(private val movieRepo: IMoviesDataSource)
    : BaseViewModel() {

    private val viewModelTag = "TopRatedMoviesTAGModel"
    val topRatedMoviesLiveData = MutableLiveData<MoviesStatus>()
    val topRatedSelectedMovieLiveData = MutableLiveData<MoviesStatus>()

    fun initialize() {
        Log.i(viewModelTag, "initialize...")
        if (topRatedMoviesLiveData.value == null) {
            fetchTopRated()
        }
    }

    fun fetchTopRated() {
        Log.i(viewModelTag, "fetchTopRated...")
        topRatedMoviesLiveData.postValue(MoviesStatus.Loading)
        launch {
            val movieResult = withContext(Dispatchers.IO) {
                Log.i(viewModelTag, "fetchTopRated IO...")
                movieRepo.getTopRatedMovies()
            }


            when (movieResult) {
                is MoviesResult.Success -> {
                    Log.i(viewModelTag, "fetchTopRated Success...")
                    topRatedMoviesLiveData.postValue(MoviesStatus.Data(movieResult.movies))
                }
                is MoviesResult.Error -> topRatedMoviesLiveData.postValue(MoviesStatus.Error)
            }
        }
    }
}
