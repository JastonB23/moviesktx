package com.example.moviektx.ui.movies.toprated

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.moviektx.data.IMoviesDataSource

class TopRatedViewModelFactory (private val movieRepo: IMoviesDataSource)
    : ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return TopRatedMoviesViewModel(movieRepo) as T
    }
}