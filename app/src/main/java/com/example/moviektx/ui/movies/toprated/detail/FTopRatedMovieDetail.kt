package com.example.moviektx.ui.movies.toprated.detail

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import com.example.moviektx.R
import com.example.common.base.BaseFragment
import kotlinx.android.synthetic.main.top_rated_detail_fragment.*

class FTopRatedMovieDetail : BaseFragment() {
    private val TAG = "FTopRatedMovieDetail"

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.top_rated_detail_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initUI()
    }

    private fun initUI() {
        Log.i(TAG, "initUI...")
        sharedViewModel.selectedMovie.observe(this@FTopRatedMovieDetail, Observer {movie ->
            if (movie == null) return@Observer

            title.text = movie.title
            textView_overview.text = String.format("%s\n\n%s\n\n%s\n\n%s",
                movie.overview, movie.overview, movie.overview, movie.overview)

            val movieImageBasUrl = "https://image.tmdb.org/t/p/w500/"

            Glide.with(this@FTopRatedMovieDetail)
                .load(movieImageBasUrl + movie.backdrop_path)
                .into(imageView_movie_backdrop)
        })

    }
}