package com.example.moviektx.ui.movies.toprated.list

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.moviektx.R
import com.example.moviektx.data.MoviesStatus
import com.example.model.local.MovieEntity
import com.example.common.base.BaseFragment
import com.example.moviektx.ui.movies.toprated.TopRatedMoviesViewModel
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.top_rated_fragment.*
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel

class FTopRatedMovies : BaseFragment() {

    private val TAG = "FTopRatedMovies"
    private val topRatedRatedAdapter: TopRatedMoviesAdapter by inject()
    private val topViewModel: TopRatedMoviesViewModel by viewModel()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.top_rated_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initUI()
        topViewModel.initialize()
    }

    private fun initUI() {
        Log.i(TAG, "initUI...")
        topRatedRatedAdapter.addViewModel(topViewModel)

        recycler_view.layoutManager = LinearLayoutManager(this@FTopRatedMovies.context)
        recycler_view.adapter = topRatedRatedAdapter
        srl_lines_status.setOnRefreshListener { topViewModel.fetchTopRated() }

        topViewModel.topRatedMoviesLiveData.observe(this@FTopRatedMovies, Observer {movieStatus ->
            if (movieStatus == null) return@Observer
            Log.i(TAG, "topRatedMoviesLiveData Observed fired...")

            when(movieStatus) {
                is MoviesStatus.Loading -> showLoading()
                is MoviesStatus.Error -> showError()
                is MoviesStatus.Data -> onTopRatedDataObserved(movieStatus.movies)
            }
        })

        topViewModel.topRatedSelectedMovieLiveData.observe(this@FTopRatedMovies, Observer {movieStatus ->
            if (movieStatus == null) return@Observer
            Log.i(TAG, "topRatedSelectedMovieLiveData Observed fired...")
            when(movieStatus) {
                is MoviesStatus.SelectedMovie -> {
                    sharedViewModel.selectedMovie.value = movieStatus.movie
                    topViewModel.topRatedSelectedMovieLiveData.value = null
                    getNavController().navigate(R.id.action_TopRated_to_TopRatedDetail)
                }
            }
        })
    }

    private fun onTopRatedDataObserved(movies: List<MovieEntity>) {
        Log.i(TAG, "onTopRatedDataObserved...")
        srl_lines_status.isRefreshing = false
        topRatedRatedAdapter.submitList(movies)
    }

    private fun showLoading() {
        Log.i(TAG, "showLoading...")
        srl_lines_status.isRefreshing = true
    }

    private fun showError() {
        Log.i(TAG, "showError...")
        srl_lines_status.isRefreshing = false
        Snackbar.make(root, R.string.error_message, Snackbar.LENGTH_LONG).show()
    }
}
