package com.example.moviektx.ui.movies.toprated.list

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.moviektx.R
import com.example.moviektx.data.MoviesStatus
import com.example.model.local.MovieEntity
import com.example.moviektx.ui.movies.toprated.TopRatedMoviesViewModel
import kotlinx.android.synthetic.main.item_movie_row.view.*

class TopRatedMoviesAdapter
    : ListAdapter<MovieEntity, TopRatedMoviesAdapter.MovieViewHolder>(
    MovieDiffUtil()
) {
    lateinit var topRatedMoviesViewModel: TopRatedMoviesViewModel

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return MovieViewHolder(
            inflater.inflate(
                R.layout.item_movie_row,
                parent,
                false
            )
        )
    }

    fun addViewModel(viewModel: TopRatedMoviesViewModel) {
        topRatedMoviesViewModel = viewModel
    }

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        holder.bind(getItem(position), ::onMovieSelected)
    }

    class MovieDiffUtil : DiffUtil.ItemCallback<MovieEntity>() {
        override fun areItemsTheSame(oldItem: MovieEntity, newItem: MovieEntity): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: MovieEntity, newItem: MovieEntity): Boolean {
            return oldItem == newItem
        }
    }

    private fun onMovieSelected(movie: MovieEntity) {
        topRatedMoviesViewModel.topRatedSelectedMovieLiveData.postValue(MoviesStatus.SelectedMovie(movie))
    }

    class MovieViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView) {

        fun bind(movie : MovieEntity, onClickMethod: (MovieEntity) -> Unit) {
            val movieImageBasUrl = "https://image.tmdb.org/t/p/w500/"

            itemView.textView_movie_title.text = movie.title
            itemView.textView_movie_rating.text = movie.vote_average.toString()
            itemView.textView_movie_desc.text = movie.overview

            Glide.with(this.itemView.context)
                    .load(movieImageBasUrl + movie.poster_path)
                    .into(itemView.imageView_movie_icon)

            itemView.setOnClickListener {
                onClickMethod(movie)
            }
        }
    }
}