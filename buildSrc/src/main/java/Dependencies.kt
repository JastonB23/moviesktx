

object Versions {
    const val compileSdk = 28
    const val minSdk = 23
    const val targetSdk = 28

    const val gradle = "3.4.0"
    const val kotlin = "1.3.21"
    const val navigation = "2.1.0-alpha02"
    const val kodein = "5.2.0"
    const val lifecycle = "2.0.0"
    const val room = "2.1.0-alpha04"
    const val retrofit = "2.4.0"
    const val retrofitAdap = "0.9.2"
    const val gson = "2.8.5"
    const val koin = "1.0.2"
    const val glide = "4.9.0"
    const val prefs = "1.0.0"

    const val appcompat = "1.1.0-alpha04"
    const val support = "28.0.0"
    const val corektx = "1.1.0-alpha05"
    const val material = "1.1.0-alpha05"
    const val constraint = "2.0.0-alpha4"
    const val recyclerview = "1.0.0"
    const val cardview = "1.0.0"
    const val threetenabp = "1.1.0"
    const val coroutines = "1.1.1"

    const val junit = "4.12"
    const val testRunner = "1.2.0-alpha04"
    const val espresso = "3.2.0-alpha04"
}

object Releases {
    val versionCode = 2
    val versionName = "1.1.0"
}

object Modules {
    val app = ":app"
    val navigation = ":navigation"
    val common = ":common"
    val model = ":data:model"
}

object Libraries {
    //KOTLIN
    const val kotlin = "org.jetbrains.kotlin:kotlin-stdlib-jdk7:${Versions.kotlin}"

    // KOIN
    const val koin = "org.koin:koin-android:${Versions.koin}"
    const val koinViewModel = "org.koin:koin-androidx-viewmodel:${Versions.koin}"
    const val koinScope = "org.koin:koin-androidx-scope:${Versions.koin}"

    // GSON
    const val gson = "com.google.code.gson:gson:${Versions.gson}"

    // RETROFIT
    const val retrofit = "com.squareup.retrofit2:retrofit:${Versions.retrofit}"
    const val retrofitConvert = "com.squareup.retrofit2:converter-gson:${Versions.retrofit}"
    const val retrofitCoroAdap = "com.jakewharton.retrofit:retrofit2-kotlin-coroutines-adapter:${Versions.retrofitAdap}"

    // GLIDE
    const val glide = "com.github.bumptech.glide:glide:${Versions.glide}"
    const val glideComp = "com.github.bumptech.glide:compiler:${Versions.glide}"

    //Coroutines
    const val coroutinesCore = "org.jetbrains.kotlinx:kotlinx-coroutines-core:${Versions.coroutines}"
    const val coroutinesAndroid = "org.jetbrains.kotlinx:kotlinx-coroutines-android:${Versions.coroutines}"

    // TREE TEN ADP
    const val threetenabp = "com.jakewharton.threetenabp:threetenabp:${Versions.threetenabp}"

}

object AndroidLibraries {
    const val appcompat = "androidx.appcompat:appcompat:${Versions.appcompat}"
    const val appsupport = "com.android.support:support-v4:${Versions.support}"
    const val coreKtx = "androidx.core:core-ktx:${Versions.corektx}"

    const val material = "com.google.android.material:material:${Versions.material}"
    const val constraintLayout = "androidx.constraintlayout:constraintlayout:${Versions.constraint}"
    const val recyclerView = "androidx.recyclerview:recyclerview:${Versions.recyclerview}"
    const val navigationui = "androidx.navigation:navigation-ui-ktx:${Versions.navigation}"
    const val navigationFrag = "androidx.navigation:navigation-fragment-ktx:${Versions.navigation}"
    const val navigationCommon = "androidx.navigation:navigation-common-ktx:${Versions.navigation}"
    const val cardView = "androidx.cardview:cardview:${Versions.cardview}"

    const val prefs = "androidx.preference:preference:${Versions.prefs}"

    // ROOM
    const val roomCompiler = "androidx.room:room-compiler:${Versions.room}"
    const val roomRunTime = "androidx.room:room-runtime:${Versions.room}"
    const val roomKtx = "androidx.room:room-ktx:${Versions.room}"
    const val roomCoroutines = "androidx.room:room-coroutines:${Versions.room}"

    // LIFE CYCLE
    const val lifecycleExten = "androidx.lifecycle:lifecycle-extensions:${Versions.lifecycle}"
    const val lifecycleViewModel = "androidx.lifecycle:lifecycle-viewmodel-ktx:${Versions.lifecycle}"
    const val lifecycleLiveData = "androidx.lifecycle:lifecycle-livedata:${Versions.lifecycle}"
    const val lifecycleRuntime = "androidx.lifecycle:lifecycle-runtime:${Versions.lifecycle}"
    const val lifecycleCompiler = "androidx.lifecycle:lifecycle-compiler:${Versions.lifecycle}"
}

object TestLibraries {
    const val junit = "junit:junit:${Versions.junit}"
    const val testRunner = "androidx.test:runner:${Versions.testRunner}"
    const val espresso = "androidx.test.espresso:espresso-core:${Versions.espresso}"
}