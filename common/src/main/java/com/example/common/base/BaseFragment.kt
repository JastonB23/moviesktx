package com.example.common.base

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController

abstract class BaseFragment : Fragment() {
    protected lateinit var sharedViewModel: SharedViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // Get the already created activity view model
        sharedViewModel = ViewModelProviders.of(activity!!).get(SharedViewModel::class.java)

        allowEnterTransitionOverlap = false
        allowReturnTransitionOverlap = false
    }

    fun getNavController() : NavController {
        return findNavController()
    }
}