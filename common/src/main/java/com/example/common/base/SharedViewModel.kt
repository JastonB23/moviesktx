package com.example.common.base

import androidx.lifecycle.MutableLiveData
import com.example.model.local.MovieEntity

/**
 * View Model response for sharing information among the fragments
 *
 * Created by James Buxton - 9th March 2019
 */
class SharedViewModel : BaseViewModel() {
    var selectedMovie = MutableLiveData<MovieEntity>()
}